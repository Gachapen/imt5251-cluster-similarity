extern crate csv;

use std::str::FromStr;
use std::cmp::PartialEq;

fn count_matches<T>(points: &Vec<T>, x: &Vec<Vec<T>>, y: &Vec<Vec<T>>) -> (u32, u32, u32, u32)
    where T: PartialEq {

    assert!(x.len() == y.len());

    fn find_cluster<T>(clusters: &Vec<Vec<T>>, point: &T) -> Option<usize>
        where T: PartialEq {

        for (i, cluster) in clusters.iter().enumerate() {
            for x in cluster {
                if *x == *point {
                    return Some(i);
                }
            }
        }

        None
    }

    let mut a = 0;
    let mut b = 0;
    let mut c = 0;
    let mut d = 0;

    for (i, p1) in points.iter().enumerate() {
        for p2 in points[i+1..].iter() {
            let xp1 = find_cluster(&x, p1).unwrap();
            let yp1 = find_cluster(&y, p1).unwrap();
            let xp2 = find_cluster(&x, p2).unwrap();
            let yp2 = find_cluster(&y, p2).unwrap();

            if xp1 == xp2 && yp1 == yp2 {
                a += 1;
            } else if xp1 != xp2 && yp1 != yp2 {
                b += 1;
            } else if xp1 == xp2 && yp1 != yp2 {
                c += 1;
            } else if xp1 != xp2 && yp1 == yp2 {
                d += 1;
            }
        }
    }

    (a, b, c, d)
}

fn jaccard_index<T>(points: &Vec<T>, x: &Vec<Vec<T>>, y: &Vec<Vec<T>>) -> f32
    where T: PartialEq {

    let (a, _, c, d) = count_matches(&points, &x, &y);

    a as f32 / (a + c + d) as f32
}

fn rand_index<T>(points: &Vec<T>, x: &Vec<Vec<T>>, y: &Vec<Vec<T>>) -> f32
    where T: PartialEq {

    let (a, b, c, d) = count_matches(&points, &x, &y);

    (a + b) as f32 / (a + b + c + d) as f32
}

fn main() {
    let mut reader = csv::Reader::from_file("data.csv").expect("No data.csv file");
    let mut samples = vec![];

    for row in reader.decode() {
        let (id, raw_clusters): (String, [String; 4]) = row.unwrap();

        let mut clusters = vec![];
        for raw_cluster in raw_clusters.iter() {
            let cluster = raw_cluster.split(',').map(|x| u32::from_str(x.trim()).unwrap()).collect::<Vec<_>>();
            clusters.push(cluster);
        }

        samples.push((id, clusters));
    }

    let index_func = rand_index;

    println!("{:?}", samples);

    let num_samples = samples.len();
    let num_humans = 6;
    let points = (1..17).collect::<Vec<u32>>();
    let mut rand_mat = vec![vec![1.0f32; num_humans]; num_humans];

    for i in 0..num_humans {
        for j in i+1..num_humans {
            let index = index_func(&points, &samples[i].1, &samples[j].1);
            rand_mat[i][j] = index;
            rand_mat[j][i] = index;
        }
    }

    for i in 0..num_humans {
        print!(";{}", i+1);
    }
    println!("");
    for (i, row) in rand_mat.iter().enumerate() {
        print!("{}", samples[i].0);
        for x in row {
            print!(";{}", x);
        }
        println!("");
    }

    println!("");

    for i in num_humans..num_samples {
        print!("{}", samples[i].0);
        for j in 0..num_humans {
            let index = index_func(&points, &samples[i].1, &samples[j].1);
            print!(";{}", index);
        }
        println!("");
    }
}
